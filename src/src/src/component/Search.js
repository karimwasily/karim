import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LeftMenu from './LeftMenu';
import SubCategory from './SubCategory'
import FiltersPage from './FiltersPage'
import MainCategoryPage from "./MainCategoryPage";
import LeftMenuMainCat from "./LeftMenuMainCat";
import { connect } from "react-redux";

import Skeleton from '../Skeleton/Skeleton'
import './category.css';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoaded: false,
            searchStart: '',
            searchNumItems: '',
            searchItem: '',
            didYouMean: [] ,
            facets: [],
            noResult: false
        }
    }
     
    componentDidMount(props) {
        this.showSearchResult()
    }
    componentDidUpdate(prevProps, prevState){
        if(prevProps !==  this.props){
            console.log('props not matched')
            this.showSearchResult()
            
        }
    } 
    showSearchResult = () => {
        let searchWord = this.props.location.search.substring(8) 
        console.log(searchWord)
        fetch(this.props.getAppSet.API.searchTerm+searchWord+'?searchSource=S')
            .then(res => res.json())
            .then(json => {
                this.setState({
                    items: json,
                    isLoaded: true,
                    searchStart: true,
                    searchNumItems:json.recordSetCount,
                    searchItem: this.props.location.search.substring(8),
                    noResult: false
                })
                if(json.FacetView[1].Entry.length > 0 ){ 
                    this.setState({facets: json.FacetView[1].Entry})
                }
                //console.log(this.state.facets)

            }).catch(
                e => {
                    this.setState({noResult: true, isLoaded: false})
                    console.log(e)
                    this.didYouMeanRslt()
                }
                //this.setState
                );
    }
    didYouMeanRslt = () => {
        fetch(this.props.getAppSet.API.searchDidYouMean+this.props.location.search.substring(8)+'?pageSize=4&terms.fl=spellCheck')
        .then(res => res.json())
        .then(json => {
            this.setState({
                didYouMean: json.suggestionView[0].entry, isLoaded: true    
            })  
            console.log(this.state.didYouMean)
        }).catch(e => console.log(e)); 
    }
    render() {
        var { isLoaded, items, facets, didYouMean } = this.state;
        console.log(didYouMean.suggestionView)
        if(!isLoaded){
            return <>
                <div className="mainDiv CategoryPage">
                <div className="featuredProducts"><div className="leftMenu"><p>&nbsp;</p></div><div className="imagesHolder"><Skeleton /></div></div>    
                </div></>
        }

        else{ 
            return(
                <div className="mainDiv CategoryPage">
                <div className="featuredProducts"> 
                {/* <div className="leftMenu"><p>&nbsp;</p></div> */}
                <div className="leftMenu">
                    {/* {this.state.facets[0].count}
                    {[facets].map((each, index) => (
                        <div>{each.count}</div>
                    ))} */}

                <p>Brands </p>
                    {[items].map((item, index) => (
                            <div key={index} className="menuHolder"> 
                                {item.FacetView ? item.FacetView.slice(1,2).map(item1 => (
                                    <div key={item1.value}>
                                    {item1.Entry.map((item2, index) => (
                            <div className="cateItem" key={index}> 
                                {item2.entryValue.length > 1 ? 
                                <Link to={`/Category/?${item2.entryValue}`}>{item2.label} </Link>
                                : 
                                <Link to={`/Category/?${item2.entryValue}&cat=Main`}>{item2.label} </Link>
                                }
                                
                            </div>
                                    ))}
                                    </div>
                                )): null} 
                            </div>
                        ))}
                </div>

                <div className="imagesHolder"> 
                <div className="searchHeading"><h1>Search results for "{this.props.location.search.substring(8)}" (
                        {this.state.noResult ? 0 : this.state.searchNumItems} 
                        &nbsp;matches.)</h1></div>
                
                <div className="didYouMean">
                    <br/>
                    {this.state.noResult ? 
                    <><p>Did you mean: </p> 
                    { [didYouMean].length > 0 ?
                        [didYouMean].map((itm,index) => (
                            <p key={index}>
                            {itm.map((each, index) => (
                                <span key={index}>
                                    <Link to={`/Search/?search=${each.term}`}>
                                    {each.term}
                                    </Link>
                                </span>                                
                             ))} 
                            </p> 
                        
                            
                        )) : null
                           }
                    </>
                    : null }
                    
                    
                     
                        
                    
                </div>
                {!this.state.noResult ?
                this.state.loaderActive ? <Skeleton /> :                    
                    [items].map(item => (
                        <div className="itemSet"  key={item.recordSetCount} > 
                        
                        {item.CatalogEntryView.map(insideItems => (
                            //if(item.recordSetTotal>1){
                            <div className="product" key={insideItems.uniqueID}>
                                <div className="name">
                                <Link to={`/Product/?${insideItems.uniqueID}`}>
                                    <div className="name"><img alt = {insideItems.name} src={`${this.props.getAppSet.serverBaseURL}${insideItems.thumbnail}`} /></div>
                                </Link>
                                </div>
                                <div className="person">{insideItems.name}</div> 
                                <div className="price">$ {insideItems.Price[0].priceValue}</div>
                                <div className="addToCart">
                                    {/* <a onClick={this.addToCartHandler.bind(this, insideItems.uniqueID)}>Add to Cart</a> */}
                                {/* <Link to={`/Product/?${insideItems.uniqueID}`}>
                                    Add to Cart
                                </Link> */}
                                </div>

                            </div>
                            //}
                        ))}    
                    </div>
                    ))
                : null}
                </div>
                </div>   

                </div>          
            )
        }
}
}
const mapStateToProps = (state) => {
    return {
        getAppSet: state.getAppSet
    }
};
export default connect(mapStateToProps, null)(Search);