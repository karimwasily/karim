import React, {Component}  from 'react';
import { parse } from 'query-string';
import {BrowserRouter as Router, Route, Link,} from 'react-router-dom'; 
import { connect } from "react-redux";
import ReactDOM from 'react-dom'; 
import LeftMenu from './LeftMenu';
import queryString from 'querystring'
import './category.css';
import Skeleton from '../Skeleton/Skeleton'
class SubCategory extends Component{
    constructor(props){
        super(props);
        this.state ={
            items: [],
            relItems: [{"longDescription":"VitaVerve's prenatal vitamin bar contains all of the vitamins and minerals found in the recommended daily dose of over-the-counter vitamin pills. Each box contains 8 bars and each bar contains 75mg of Omega-3 DHA to help your baby's development. Each pack contains six 4 ounce bars.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3801.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12768","shortDescription":"Providing you with essential prenatal vitamins.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"8.0","priceDescription":"I"}],"name":"VitaVerve Prenatal Vitamin Bar","partNumber":"HVT038_3801","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3801.jpg","uniqueID":"12768","productType":"ProductBean"},{"longDescription":"VitaVerve's Essential Seeds Bar contains eight kinds of seeds, providing essential amino acids to lower cholesterol and reduce the risk of cancer. Each pack contains twelve bars.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3802.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12770","shortDescription":"Improve your heart health.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"10.0","priceDescription":"I"}],"name":"VitaVerve Essential Seeds Bar","partNumber":"HVT038_3802","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3802.jpg","uniqueID":"12770","productType":"ProductBean"},{"longDescription":"Essential for a healthy immune system, Cod Liver oil is high in Vitamin A and D3. Eases symptoms of rheumatoid arthritis while supporting cardiovascular health. Bottle contains 120 capsules.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3803.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12772","shortDescription":"Boost your immune system.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"18.99","priceDescription":"I"}],"name":"Cod Liver Oil Caps","partNumber":"HVT038_3803","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3803.jpg","uniqueID":"12772","productType":"ProductBean"},{"longDescription":"Contains eight essential vitamins. Helps metabolize carbohydrates, proteins and fats and promotes a healthy nervous system. Each bottle contains 90 capsules.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3804.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12774","shortDescription":"For a healthy body and mind.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"13.25","priceDescription":"I"}],"name":"VitaVerve Vitamin B Complex 100 mg","partNumber":"HVT038_3804","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3804.jpg","uniqueID":"12774","productType":"ProductBean"},{"longDescription":"Aids in blood cell growth and health. Recommended for both pregnant and new mothers for improving fetal and embryonic health. Each bottle contains 40 gel caps.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3805.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12776","shortDescription":"Vital for pregnant women.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"9.49","priceDescription":"I"}],"name":"Folic Acid 1 mg","partNumber":"HVT038_3805","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3805.jpg","uniqueID":"12776","productType":"ProductBean"},{"longDescription":"A natural herb known to increase energy, stamina as well as enhancing the immune and cardiovascular system. Helps fight fatigue. Each bottle contains 70 gel caps.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3806.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12778","shortDescription":"Natural energy booster.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"23.49","priceDescription":"I"}],"name":"Red Ginseng 250 mg","partNumber":"HVT038_3806","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3806.jpg","uniqueID":"12778","productType":"ProductBean"},{"longDescription":"Calcium is an essential mineral for the healthy development of bones and teeth. Helps combat osteoporosis and helps relieve premenstrual syndrome. Each bottle contains 50 pills.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3807.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12780","shortDescription":"For healthy bones.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"6.69","priceDescription":"I"}],"name":"Calcium 700 mg","partNumber":"HVT038_3807","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3807.jpg","uniqueID":"12780","productType":"ProductBean"},{"longDescription":"Essential for the development of a healthy immune system in young children. Strengthens the cardiovascular system and regulates blood glucose levels. Bottle contains 70 gel caps.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3808.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12782","shortDescription":"Provides energy and boosts stamina.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"10.59","priceDescription":"I"}],"name":"Vitamin B6 (Pyridoxine) 250 mg","partNumber":"HVT038_3808","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3808.jpg","uniqueID":"12782","productType":"ProductBean"},{"longDescription":"Composed of 13 vitamins and minerals, HealthyBoost's prenatal multivitamin offers a perfectly balanced supplement for expecting mothers. Each bottle contains 80 pills.","buyable":"true","thumbnail":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/200x310/hvt038_3809.jpg","resourceId":"https://192.168.17.91:5443/wcs/resources/store/1/productview/byId/12784","shortDescription":"Multivitamin for pregnant and new mothers.","storeID":"10501","Price":[{"priceUsage":"Offer","priceValue":"9.29","priceDescription":"I"}],"name":"Healthy Boost Prenatal Multivitamin +","partNumber":"HVT038_3809","parentCategoryID":"10032","fullImage":"/wcsstore/ExtendedSitesCatalogAssetStore/images/catalog/health/hvt038_vitamins/646x1000/hvt038_3809.jpg","uniqueID":"12784","productType":"ProductBean"}],
            isLoaded: false,
            categoryCalled: '', 
            loaderActive: true,
            subCat: '',
            priceOrPrice: ''
        }
        this.allItems = ''
    }
    componentDidUpdate(prevProps, prevState, snapshot){ 
      //  console.log(this.props.callCat.categoryCall+"<<subCategoryPage")
       // console.log("subCategoryPage >>"+this.props.callCat.subcat)
    if(prevProps !== this.props){
            let filterUrl = this.props.callCat.subSrch
            if(filterUrl.indexOf('StartValue')>0 || filterUrl.indexOf('EndValue')>0){
                console.log('filter is called')
                this.filterData()
            }
            if(filterUrl.indexOf('Filter')>0){
                this.reloadDataFromServer()
            }
            console.log(this.props.callCat.subSrch)
            if(this.props.callCat.categoryCall != this.state.categoryCalled){            
                this.getDataFromServer(this.props.callCat.categoryCall);
                
                this.setState({
                    categoryCalled: this.props.callCat.categoryCall,
                    loaderActive: true
                })
            }
        }
    }
    reloadDataFromServer = () => {
        let getLoc = this.props.callCat.subSrch.indexOf("Filter")+7
        let getSrch = this.props.callCat.subSrch.substring(getLoc)
        let getCate = this.props.callCat.subSrch.substring(1,6)
    
        this.setState({loaderActive:'<div><img width="300" height="200" src="/Images/loader.gif"/></div>'}) 
        let getUrl = `http://192.168.17.91:3737/search/resources/store/1/productview/byCategory/${getCate}?facet=${getSrch}`
        console.log(getUrl)
        //`url2: 'http://192.168.17.91:3737/search/resources/store/1/productview/byCategory/10015?facet=Mayflower'`  
        fetch(getUrl) 
        .then(res => res.json(
            
        ))
        .then(json => {
            
            this.setState({
                isLoaded: true,
                items: json.catalogEntryView,
                allItems: json.catalogEntryView,
                loaderActive: false,
                priceOrPrice: 'priceSmall',
            })
            this.allItems = json.catalogEntryView
            
        }).catch(e => console.log(e));
        this.setState({loaderActive:''})
    }
    filterData = () => {
        let filterUrl = this.props.callCat.subSrch
        if(filterUrl.indexOf('StartValue')>0 || filterUrl.indexOf('EndValue')>0){
            let getStartValue = ''
            let getEndValue = ''
            if(filterUrl.indexOf('StartValue')>0 && filterUrl.indexOf('EndValue')>0){
                getStartValue = filterUrl.substr(filterUrl.indexOf('StartValue')+11, 3)
                getEndValue = filterUrl.substr(filterUrl.indexOf('EndValue')+9, 3)
                this.filteringBy(getStartValue, getEndValue) 
            }
            else if(filterUrl.indexOf('EndValue')>0){ // below then 100
                getStartValue = ''
                getEndValue = filterUrl.substr(filterUrl.indexOf('EndValue')+9, 3)
                this.filteringBy(getStartValue, getEndValue) 
            }
            else if(filterUrl.indexOf('StartValue')>0){ // above hten 500
                getStartValue = filterUrl.substr(filterUrl.indexOf('StartValue')+11, 3)
                getEndValue = ''
                this.filteringBy(getStartValue, getEndValue) 
            }
                             
        }
    }
    filteringBy = (startValue, EndValue) => {
        let endValueRslt = [];
        if(parseFloat(startValue) && parseFloat(EndValue)){
            console.log('success')
            endValueRslt = this.allItems.filter(each  =>  {
                if(each.Price){
                    if(each.Price[0].priceValue > parseFloat(startValue) && each.Price[0].priceValue < parseFloat(EndValue)){
                        return true
                    }
                }
                else if(each.price){
                    if(each.price[0].value > parseFloat(startValue) && each.price[0].value < parseFloat(EndValue)){
                         return true
                     }
                }
            }); 
        }
        if(parseFloat(startValue) && EndValue === ''){ //above then 500
            endValueRslt = this.allItems.filter(each  =>  {
                if(each.Price){
                    if(each.Price[0].priceValue > parseFloat(startValue) ){
                        return true
                    }
                }
                else if(each.price){
                    if(each.price[0].value > parseFloat(startValue) ){
                        return true
                    }
                }
            });
        }
        
        if(startValue === '' && parseFloat(EndValue) ){ //below then 100
            endValueRslt = this.allItems.filter(each  =>  {
                if(each.Price){
                    if(each.Price[0].priceValue < parseFloat(EndValue)){
                        return true
                    }
                }
                else if(each.price){
                    if(each.price[0].value < parseFloat(EndValue)){
                         return true
                     }
                }
            });
        }
        this.setState({items: endValueRslt})
        console.log(this.state.allItems)

    }
    componentDidMount(props){
        this.setState({
            categoryCalled: this.props.callCat.categoryCall
        }) 
        console.log('didMount subCat')
        this.getDataFromServer(this.props.callCat.categoryCall);  
    } 
    getDataFromServer = (getCat) => { 
        
        this.setState({loaderActive:'<div><img width="300" height="200" src="/Images/loader.gif"/></div>'}) 
        let getUrl = this.props.getAppSet.API.productById+getCat
        //`https://192.168.17.91:5443/wcs/resources/store/1/categoryview/byParentCategory/5`  
        fetch(getUrl) 
        .then(res => res.json(
            
        ))
        .then(json => {
            
            this.setState({
                isLoaded: true,
                items: json.CatalogEntryView,
                relItems: json.CatalogEntryView,
                loaderActive: false,
                priceOrPrice: 'PriceCaps'
            })
            this.allItems = json.CatalogEntryView
            console.log(this.allItems)
        }).catch(e => console.log(e));
        this.setState({loaderActive:''}) 
        
    }
    addToCartHandler(id){
        console.log(id+'id inside ');
    }

    intSortBy = fn => (a, b) => {
        const fa = parseInt(fn(a));
        const fb = parseInt(fn(b));
        return -(fa < fb) || +(fa > fb);
      };

    stringSortBy = fn => (a, b) => {
        const fa = fn(a);
        const fb = fn(b);
        return -(fa < fb) || +(fa > fb);
      };

      sortItems = (event) => {
         
        let getItemsBy = o => o.searchByVar;
        let getSortResult = ''
        let oldItemsRel = this.state.items
        if(event.target.value === 'name'){ 
            console.log(event.target.value)
            getItemsBy = o => o.name;
            const sortByRequired = this.stringSortBy(getItemsBy);
            getSortResult = this.state.items.sort(sortByRequired);
            this.setState({items: getSortResult})
        }
        else if(event.target.value === 'high'){
            if(this.state.priceOrPrice === 'PriceCaps'){
                getItemsBy = o => o.Price[0].priceValue;
            }
            else{
                getItemsBy = o => o.price[0].value;
            }
            console.log(event.target.value)
            const sortByRequired = this.intSortBy(getItemsBy);
            getSortResult = this.state.items.reverse(sortByRequired);
            this.setState({items: getSortResult})
        }
        else if(event.target.value === 'low'){
            if(this.state.priceOrPrice === 'PriceCaps'){
                getItemsBy = o => o.Price[0].priceValue;
            }
            else{
                getItemsBy = o => o.price[0].value;
            }
            const sortByRequired = this.intSortBy(getItemsBy);
            getSortResult = this.state.items.sort(sortByRequired);
            this.setState({items: getSortResult})
        }
        else if(event.target.value === 'rel'){
            getSortResult = this.state.relItems.sort()
            console.log(this.state.relItems)
            this.setState({items: getSortResult})
        }
       
        // const sortByRequired = this.sortBy(getItemsBy);
        // var getSorResult = this.state.items.sort(sortByRequired);
        
        //console.log(check);
      }

    render(){
       // console.log('welcome to subCate page')
        var { isLoaded, items } = this.state;
        var dmc = JSON.stringify(items);
        //tems.reverse()
        console.log(items)
        if(!isLoaded){
            return <Skeleton />
        }
        else{ 
            return(
                <>
                <div>
                    {/* <p>{dmc}</p> */}
                    Sort By: <select onChange={this.sortItems}>
                        <option value="rel">Relevance</option> 
                        <option value="name">Name</option>
                        <option value="low">Price (Low to High)</option>
                        <option value="high">Price (High to Low)</option>
                    </select>
                </div>
                <div>
                {this.state.loaderActive ? <Skeleton /> :                    
                    //[items].map(item => (
                        // <div  key={item.recordSetCount} > 
                        this.state.items.map((insideItems, index) => (
                            //if(item.recordSetTotal>1){
                            <div className="product" key={insideItems.uniqueID}>
                                <div className="name">{index}
                                <Link to={`/Product/?${insideItems.uniqueID}`}>
                                    <div className="name"><img alt = {insideItems.name} src={`${this.props.getAppSet.serverBaseURL}${insideItems.thumbnail}`} /></div>
                                </Link>
                                </div>
                                <div className="person">{insideItems.name}</div> 
                                <div className="price">$ {
                                    insideItems.Price ? insideItems.Price[0].priceValue : insideItems.price ? insideItems.price[0].value : null
                                }</div>
                                <div className="addToCart">
                                    {/* <a onClick={this.addToCartHandler.bind(this, insideItems.uniqueID)}>Add to Cart</a> */}
                                {/* <Link to={`/Product/?${insideItems.uniqueID}`}>
                                    Add to Cart
                                </Link> */}
                                </div>

                            </div>
                            //}
                        ))    
                    // </div>
                    //))
                }
                </div>
                </>             
            )
        }
}
}
const mapStateToProps = (state) => {
    return { 
        getAppSet: state.getAppSet
    }
};
export default connect(mapStateToProps, null)(SubCategory);