import React, { Component } from "react";
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'; 
import './category.css';
import Skeleton from '../Skeleton/Skeleton'

class FiltersPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            items: [],
            allItems: [],
            isLoaded: false,
            categoryCalled: '', 
            loaderActive: true,
            subCat: ''
        }
        this.allItems= ''
    }
    componentDidUpdate(prevProps, prevState, snapshot){ 
        console.log(this.props.callCat.subSrch.indexOf('EndValue')>0)
        
        //console.log(this.props)
        if(prevProps !== this.props){
            this.callOrNot()   
        }         
    }
    callOrNot = () => {
        //console.log(filterUrl)
        let filterUrl = this.props.callCat.subSrch
        if(filterUrl.indexOf('StartValue')>0 || filterUrl.indexOf('EndValue')>0){
            let getStartValue = ''
            let getEndValue = ''
            if(filterUrl.indexOf('StartValue')>0 && filterUrl.indexOf('EndValue')>0){
                getStartValue = filterUrl.substr(filterUrl.indexOf('StartValue')+11, 3)
                getEndValue = filterUrl.substr(filterUrl.indexOf('EndValue')+9, 3)
                this.filterData(getStartValue, getEndValue) 
            }
            else if(filterUrl.indexOf('EndValue')>0){ // below then 100
                getStartValue = ''
                getEndValue = filterUrl.substr(filterUrl.indexOf('EndValue')+9, 3)
                this.filterData(getStartValue, getEndValue) 
            }
            else if(filterUrl.indexOf('StartValue')>0){ // above hten 500
                getStartValue = filterUrl.substr(filterUrl.indexOf('StartValue')+11, 3)
                getEndValue = ''
                this.filterData(getStartValue, getEndValue) 
            }
                             
        }
        else{
        this.getDataFromServer(this.props.callCat)
        }
    }
    filterData = (startValue, EndValue) => {
        console.log(startValue+EndValue)
        let getValues = this.allItems
        let endValueRslt = [];
        if(parseFloat(startValue) && parseFloat(EndValue)){
            console.log('success')
            endValueRslt = this.allItems.filter(each  =>  {
                if(each.price[0].value > parseFloat(startValue) && each.price[0].value < parseFloat(EndValue)){
                   // console.log(each.price[0].value+each.price[0].value)
                    return true
                }
            }); 
        }
        if(parseFloat(startValue) && EndValue === ''){ //above then 500
            endValueRslt = this.allItems.filter(each  =>  {
                if(each.price[0].value > parseFloat(startValue) ){
                    return true
                }
            });
        }
        
        if(startValue === '' && parseFloat(EndValue) ){ //below then 100
            endValueRslt = this.allItems.filter(each  =>  {
                if(each.price[0].value < parseFloat(EndValue)){
                   // console.log(each.price[0].value+each.price[0].value+'below 100 ')
                    return true
                }
            });
        }
        this.setState({items: endValueRslt})
        console.log(this.state.allItems)
    }
    componentDidMount(props){
        console.log('didMount')
        this.setState({
            categoryCalled: this.props.callCat.categoryCall
        })
        console.log(this.state.items.length)
        this.getDataFromServer(this.props.callCat.categoryCall);  
        //this.callOrNot()
    } 
    getDataFromServer = (getC) => { 
        let getLoc = this.props.callCat.subSrch.indexOf("Filter")+7
        let getSrch = this.props.callCat.subSrch.substring(getLoc)
        let getCate = this.props.callCat.subSrch.substring(1,6)
    
        this.setState({loaderActive:'<div><img width="300" height="200" src="/Images/loader.gif"/></div>'}) 
        let getUrl = `http://192.168.17.91:3737/search/resources/store/1/productview/byCategory/${getCate}?facet=${getSrch}`
        console.log(getUrl)
        //`url2: 'http://192.168.17.91:3737/search/resources/store/1/productview/byCategory/10015?facet=Mayflower'`  
        fetch(getUrl) 
        .then(res => res.json(
            
        ))
        .then(json => {
            
            this.setState({
                isLoaded: true,
                items: json.catalogEntryView,
                allItems: json.catalogEntryView,
                loaderActive: false
            })
            this.allItems = json.catalogEntryView
            
        }).catch(e => console.log(e));
        this.setState({loaderActive:''}) 
        
    } 
    render(){
        var { isLoaded, items } = this.state; 
       // console.log('getItems below')
         //   console.log(this.state.items)      
        return(
            <div>
              {
              items.map((item, index) => (
                        <div key={index} className="product"> 
                            {/* {item.catalogEntryView ? item.catalogEntryView.map(insideItems => (
                                <div className="product" key={insideItems.uniqueID}> */}
                                <div className="name">
                                <Link to={`/Product/?${item.uniqueID}`}>
                                    <div className="name"><img alt = {item.name} src={`https://192.168.17.91:8443${item.thumbnail}`} /></div>
                                </Link>
                                </div>
                                <div className="person">{item.name}</div> 
                                <div className="price">$ {item.price[0].value}</div>
                                <div className="addToCart">
                                    
                                </div>

                            {/* </div>
                            )): null}  */}
                        </div>
                    ))
                    
                    }
              
              
              {/* {[items].map((item, index) => (
                        <div key={index}> 
                            {item.catalogEntryView ? item.catalogEntryView.map(insideItems => (
                                <div className="product" key={insideItems.uniqueID}>
                                <div className="name">
                                <Link to={`/Product/?${insideItems.uniqueID}`}>
                                    <div className="name"><img alt = {insideItems.name} src={`https://192.168.17.91:8443${insideItems.thumbnail}`} /></div>
                                </Link>
                                </div>
                                <div className="person">{insideItems.name}</div> 
                                <div className="price">$ {insideItems.price[0].value}</div>
                                <div className="addToCart">
                                    
                                </div>

                            </div>
                            )): null} 
                        </div>
                    ))} */}
            </div>             
        )
    }
}
export default FiltersPage 