import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import queryString from 'querystring'

class LeftMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoaded: false,
            categoryCalled: '',
            isBrand: 0,
            portion1: [], 
            portion2: [],
            portion: []
        };
        //console.log(props)
    }
    componentDidMount(props) {

        this.updateCateCall(this.props.callFromSubCat)

    }
    lastCall = '';
    componentDidUpdate(props) {
        if (this.props.callFromSubCat.categoryCall != this.lastCall && this.props.callFromSubCat.subcat.trim() == '') {
            this.updateCateCall(props)
            this.lastCall = this.props.callFromSubCat.categoryCall
        }
    }
    updateCateCall = (props) => {
        console.log(">>>>>>>>>>>>>>>>>" + this.props.callFromSubCat.categoryCall)
        fetch('https://192.168.17.91:5443/wcs/resources/store/1/productview/byCategory/' + this.props.callFromSubCat.categoryCall)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    items: json,
                    portion: json.FacetView[0],
                    portion1: json.FacetView[0]
                })
                console.log(json.FacetView[0].name)
                if(json.FacetView[1].name === 'ManufacturerName'){
                    this.setState({
                        portion: json.FacetView[1]
                    })
                    
                    console.log('if')
                    console.log(this.state.portion)
                }
                else{
                    this.setState({
                        portion: json.FacetView[0]
                    })
                    console.log('else')
                    console.log(this.state.portion)
                }
            }).catch(e => console.log(e));

    }
    render() {
        //console.log(items) JSON.stringify(items)
        var { isLoaded, items, portion, portion1 } = this.state;
        console.log(this.state.portion1)
        if (!isLoaded) {
            return <div>Loading...</div>
        }
        else {
            return (
                <div className="menuHolder">

                    {  
                    this.state.items.FacetView ? 
                    <>
                    <div>
                        <p>Brands:</p>
                    {[portion].map(item1 => (
                        <div key={item1.value}>
                            {/* {item1.name} */}
                            {item1.Entry.map((item2, index) => (
                                <div className="cateItem" key={index}>
                                    <Link to={`/Category/?${this.state.items.CatalogEntryView[0].parentCategoryID}&cat=SubCat&Filter=${item2.label}`}>{item2.label} </Link>
                                </div>
                            ))}
                        </div>
                    ))} 
                    </div>
                       <br/>         
                    <div>
                    <p>Prices:</p>
                        <div className="cateItem">
                            <Link to={`?${this.state.items.CatalogEntryView[0].parentCategoryID}&cat=SubCat&EndValue=100`}>Less then 100</Link>
                        </div>
                        <div className="cateItem">
                            <Link to={`?${this.state.items.CatalogEntryView[0].parentCategoryID}&cat=SubCat&StartValue=100&EndValue=200`}>Between $100 and $200</Link>
                        </div>
                        <div className="cateItem">
                            <Link to={`?${this.state.items.CatalogEntryView[0].parentCategoryID}&cat=SubCat&StartValue=200&EndValue=300`}>Between $200 and $300</Link>
                         </div>
                        <div className="cateItem">
                            <Link to={`?${this.state.items.CatalogEntryView[0].parentCategoryID}&cat=SubCat&StartValue=300&EndValue=400`}>Between $300 and $400</Link>    
                        </div>
                        <div className="cateItem">
                            <Link to={`?${this.state.items.CatalogEntryView[0].parentCategoryID}&cat=SubCat&StartValue=400&EndValue=500`}>Between $400 and $500</Link>                        
                        </div>
                        <div className="cateItem">
                            <Link to={`?${this.state.items.CatalogEntryView[0].parentCategoryID}&cat=SubCat&StartValue=500`}>More than $500</Link>
                        </div>
                    {
                    // [portion1].map(item1 => (
                    //     <div key={item1.value}>
                    //         {/* {item1.name} */}
                    //         {item1.Entry.map((item2, index) => (
                    //             <div className="cateItem" key={index}>
                    //                 <Link to={`/Category/?${this.state.items.CatalogEntryView[0].parentCategoryID}&cat=SubCat&Filter=${item2.label}`}>{item2.label} </Link>
                    //             </div>
                    //         ))}
                    //     </div>
                    // ))
                    } 
                    </div>
                    </>            
                    : null
                    }
                </div>
            )
        }
    }

}
export default LeftMenu;