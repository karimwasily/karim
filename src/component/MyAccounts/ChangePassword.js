import React, { Component } from 'react'; 
import { connect } from 'react-redux';
class ChangePassword extends Component{
    constructor(props){
        super(props)
        this.state={
            currentPass: '',
            changePass: '',
            verifyPass: '',
            errorMsg: ''
        }
    }
    changeHandler = (event) => {
        this.setState({[event.target.name] : event.target.value})

    }
    submitForChange = () =>{
        if(this.state.currentPass.trim() === ''){
            alert('Please type current password')
            return false
        }
        if(this.state.changePass.trim() === ''){
            alert('Please type change password')
            return false
        }
        if(this.state.verifyPass.trim() === ''){
            alert('Please type verify password')
            return false
        }
        if(this.state.changePass === this.state.verifyPass){
            console.log('iside condi')
            this.gotoChangePass()
        }
        else{
            this.setState({errorMsg: 'The verify password you entered did not match your password. Type your password in the Verify password field and try again.'})
        } 
    }
    gotoChangePass = () =>{ 
            const payloads = {
                logonPassword: this.state.changePass,
                logonPasswordVerify: this.state.verifyPass,
                logonId: this.props.getEmail
            } 
            fetch(this.props.getAppSet.API.UpdatesUserpassword,{
                method: 'PUT',
                headers: {
                    'accept': 'application/json',
                    'Content-type': 'application/json',
                    'WCToken':  this.props.getWCToken,
                    'WCTrustedToken': this.props.getWCTrustedToken
                },
                body: JSON.stringify(payloads)
            })
            .then(res=>res.json())
            .then((data)=>{
                 console.log(data)
                 if(data.errors){
                     this.setState({errorMsg: data.errors[0].errorMessage})
                 }
                 else if(data.resourceName){
                    this.setState({errorMsg: 'Your Password is successfully changed!'})
                 }
            },
            (error) => {
                this.setState({
                    isAuthenticated: false,
                    errorMsg: 'Either your user name or password is wrong'
                });
                console.log(this.state.isAuthenticated+"Error Data>>"+error);
            }); 
    }
    render(){
        return(
            <div>
                <h2>Change Password</h2>
                {/* <p className="headAccInfo">Account Information</p> */}
                <hr />
                <div className="AccountInfo changePass">
                     <table>
                         <thead></thead>
                         <tbody>
                             <tr>
                                 <td>
                                     <label>Current password:</label><br/>
                                     <input type="password" name="currentPass" onChange={this.changeHandler}/>
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     <label>Password:</label><br/>
                                     <input type="password" name="changePass" onChange={this.changeHandler} />
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     <label>Verify password:</label><br/>
                                     <input type="password" name="verifyPass" onChange={this.changeHandler} />
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     <hr/>
                                     <p className="errorMsg">{this.state.errorMsg}</p><br/>
                                     <button onClick={this.submitForChange}>Submit </button>
                                 </td>
                             </tr>
                         </tbody>
                     </table>
                     <br/>
                </div>
                
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        getWCToken : state.userToken.WCToken,
        getWCTrustedToken: state.userToken.WCTrustedToken,
      getResourceName: state.userToken.resourceName,
      getEmail: state.userToken.email,

      getAppSet: state.getAppSet
    }
  }
const mapDispatchToProps = (dispatch) =>{
     return{
        changePass: (email, resourceName,  braintreeToken, tokn, userId, WCTrustedToken, personalizationID ) => {
            dispatch({
                    type: 'LOGED_USER', payloads: {email, resourceName, braintreeToken,  tokn, userId, WCTrustedToken, personalizationID}
                })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);