import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { ChangePassword } from "./ChangePassword";
class MyAccountMenu extends Component{
    constructor(props){
        super(props);
        this.state={
            activeIndex: 0
        }
    }
    handlerClasses = (index, e) => {
       this.setState({activeIndex: index})
    }
    render(){
        return(
            <div className="myAccountLinks">
                <p className="subHead">SETTINGS</p>
                <Link className={this.state.activeIndex === 0 ? 'active' : null} onClick={this.handlerClasses.bind(this, 0)} to="/MyAccount">Personal Information</Link>
                <Link className={this.state.activeIndex === 1 ? 'active' : null} onClick={this.handlerClasses.bind(this, 1)} to={`?UserPasswordUpdate`}>Change Password</Link>
                <Link className={this.state.activeIndex === 2 ? 'active' : null} onClick={this.handlerClasses.bind(this, 2)} to="#">Address Book</Link>
                <Link className={this.state.activeIndex === 3 ? 'active' : null} onClick={this.handlerClasses.bind(this, 3)} to="#">Quick Checkout Profile</Link>
                <hr/>
                <p className="subHead">ORDERS</p>
                <Link className={this.state.activeIndex === 4 ? 'active' : null} onClick={this.handlerClasses.bind(this, 4)} to="#">Order History</Link>
                {/* <Link to="#">Account Information</Link>
                <Link to="#">Stored Payment Methods</Link>
                <Link to="#">Billing Agreements</Link>
                <hr/>
                <Link to="#">My Product Reviews</Link>
                <Link to="#">Newsletter Subscriptions</Link> */}
            </div>
        )
    }
}
export default MyAccountMenu;