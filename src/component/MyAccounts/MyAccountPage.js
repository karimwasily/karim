import React, { Component } from 'react'; 
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
class MyAccountPage extends Component{
    constructor(props){
        super(props)
        this.state={
            userDetails: ''
        }
    }
    componentDidMount(props){
        this.fetchUserDetails()
    }
    fetchUserDetails = () => {
        fetch(this.props.getAppSet.API.userDetails,{
            method: 'GET',
            headers: { 
                'accept': 'application/json',
                'Content-type': 'application/json',
                'WCToken':  this.props.getWCToken,
                'WCTrustedToken': this.props.getWCTrustedToken
            }
        })
        .then(res => res.json(
            
        ))
        .then(json => {
             
            this.setState({userDetails: json})
            
        }).catch(e => console.log(e));
    }
    render(){
        return(
            <div className="myAccountMainPage">
                <h3>My Account Summary</h3>
                <br/>
                <p><b>Welcome, Karim Wasily</b></p>
                <p >This is your account summary. You can change your personal information and manage the options available for your account.</p>
                <hr />
                <br/>
                <h4>Personal information</h4>
                <div className="AccountInfo">
                    <table className="personalInfoTbl">
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>Karim Wasily</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>Shumail Arcade</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>Karachi</td>
                            </tr>
                            <tr>
                                <td>E-Mail</td>
                                <td>KarimWasily@gmail.com</td>
                            </tr>
                        </tbody>
                    </table>
                    <Link to="">Edit</Link>
                </div>
                <br/>
                <hr/>
                <br/>
                <p className="headAccInfo">Recent Order History</p>
                <hr />
                <div className="AccountInfo">
                <table className="orderHistoryTbl">
                        <thead>
                            <tr>
                                <th>Order Number</th>
                                <th>Order Date</th>
                                <th>Status</th>
                                <th>Total Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Order Number</td>
                                <td>Order Date</td>
                                <td>Status</td>
                                <td>Total Price</td>
                                <td>Actions</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colSpan="5">
                                    <Link to="#">View all Orders</Link>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div><br/>
                <br/>
                <br/>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        getWCToken : state.userToken.WCToken,
        getWCTrustedToken: state.userToken.WCTrustedToken,
        geUserId: state.userToken.userId,

        getAppSet: state.getAppSet
    }
  }
export default connect(mapStateToProps, null)(MyAccountPage);